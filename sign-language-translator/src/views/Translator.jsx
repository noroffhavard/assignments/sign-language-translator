import { useState } from "react"
import { translationAdd } from "../api/translation"
import TranslateForm from "../components/Translate/TranslateForm"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"

const Translator = () => {

    const { user, setUser } = useUser()
    const [translatedText, setTranslatedText] = useState()

    const handleTranslation = async inputToTranslate => {
        if (inputToTranslate.length > 40) {
            return alert("The translator can only translate maximum 40 letters")
        }

        const [ error, updatedUser ] = await translationAdd(user, inputToTranslate)
        if (error !== null) {
            return
        }
        // Method to keep the UI state and server state in sync
        storageSave(STORAGE_KEY_USER, updatedUser)
        // Method to update context state
        setUser(updatedUser)

        const translateWord = inputToTranslate.split("").map((char, index) => {
            if (char === " ") {
                return <span className="span_space">______</span>
            } else {
                const character = char.toLowerCase()
                const image = `/img/${character}.png`
                return (
                    <img src={image} alt={character} key={index + "-" + character} width="80"/>
                )
            }
        })
        setTranslatedText(translateWord)
    }

    return (
        <>
            <h1>Translator</h1>
            <section>
                <TranslateForm onTranslation={ handleTranslation } />
            </section>
            {translatedText && <p>{translatedText}</p>}
        </>
    )
}
export default withAuth(Translator)