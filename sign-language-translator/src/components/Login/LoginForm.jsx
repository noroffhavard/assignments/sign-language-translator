import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { loginUser } from "../../api/user"
import { storageSave } from "../../utils/storage"
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import "./LoginForm.css"

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    // Hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    // Local storage
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    // Side effects
    useEffect(() => {
        if (user !== null) {
            navigate('/translator')
        }
    }, [user, navigate])

    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }

    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === "required") {
            return <span>Username is required</span>
        }

        if (errors.username.type === "minLength") {
            return <span>Minimum username length is three characters</span>
        }
    })()

    return (
        <>
            <div id="lost-in-translation-welcome">
                <img src="/img/Logo-Hello.png" width={"200"} alt={"welcome"}></img>
                <h1>Lost in Translation</h1>
                <div id="input-login">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <input
                            id="username-input"
                            type="text"
                            placeholder="What's your name?"
                            {...register("username", usernameConfig)}
                        />
                        <button id="continue" type="submit" disabled={loading}>Continue</button>
                        <p>{errorMessage}</p>
                        {loading && <p>Logging in...</p>}
                        {apiError && <p>{apiError}</p>}
                    </form>
                </div>
            </div>
        </>
    )
}

export default LoginForm