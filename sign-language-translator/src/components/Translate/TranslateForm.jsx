import { useForm } from "react-hook-form"

const TranslateForm = ( {onTranslation} ) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = ({inputToTranslate}) => {
        onTranslation(inputToTranslate)
    }

    return (
        <>
            <form onSubmit={ handleSubmit(onSubmit) }>
                <input type="text" {...register("inputToTranslate") }
                    placeholder="What do you want to translate?"/>
                <button type="submit">Translate</button>
            </form>
        </>
    )
}
export default TranslateForm