import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({translations}) => {
    const translationList = translations.slice((translations.length - 10), translations.length).map(
        (translation, index) => <ProfileTranslationHistoryItem key={ index + "-" + translation } translation={translation} />
        )

    return (
        <section>
            <h4>Translations</h4>
            <ul> {translationList} </ul>
        </section>
    )
}
export default ProfileTranslationHistory